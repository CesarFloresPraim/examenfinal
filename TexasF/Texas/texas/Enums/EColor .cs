﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas.Enums
{
    public enum Color { Roja, Negra }
    static class EColor
    {
        public static string getString(this Color color)
        {
            switch (color)
            {
                case Color.Negra:
                    return "Negra";
                case Color.Roja:
                    return "Roja";
                default:
                    return "NA";
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas.Enums
{
    public enum Palo {Trebol, Diamante, Espadas, Corazon}
    static class EPalo
    {
        public static string getString(this Palo palo)
        {
            switch (palo)
            {
                case Palo.Diamante:
                    return "Diamante";
                case Palo.Corazon:
                    return "Corazom";
                case Palo.Espadas:
                    return "Espadas";
                case Palo.Trebol:
                    return "Trebol";
                default:
                    return "NA";
            }

        }
    }
}

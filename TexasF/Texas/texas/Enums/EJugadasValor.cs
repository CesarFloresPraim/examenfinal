﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas.Enums
{
    public enum JugadasValor { CartaAlta, Par, Par2, Tercia, Escalera, Color, Full, Poquer, Corrida, FlorImperial }

    static class EJugadasValor
    {
        
            public static int getInt(this JugadasValor jugadasValor)
            {
                switch (jugadasValor)
                {
                
                    case JugadasValor.CartaAlta:
                        return 0;
                    case JugadasValor.Par:
                        return 1;
                    case JugadasValor.Par2:
                        return 2;
                    case JugadasValor.Tercia:
                        return 3;
                    case JugadasValor.Escalera:
                        return 4;
                    case JugadasValor.Color:
                        return 5;
                    case JugadasValor.Full:
                        return 6;
                    case JugadasValor.Poquer:
                        return 7;
                    case JugadasValor.Corrida:
                        return 8;
                    case JugadasValor.FlorImperial:
                        return 9;
                    default:
                        return 0;
                        
                }
            }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas.Enums
{
    
    public enum JugadasNombre { CartaAlta,Par, Par2,Tercia,Escalera, Color, Full, Poquer, Corrida, FlorImperial}
    static class EJugadasNombre
    {
        public static string getString(this JugadasNombre jugadasNombre)
        {
            switch (jugadasNombre)
            {
                case JugadasNombre.CartaAlta:
                    return "CartaAlta";
                case JugadasNombre.Par:
                    return "Par";
                case JugadasNombre.Par2:
                    return "Pares2";
                case JugadasNombre.Tercia:
                    return "Tercia";
                case JugadasNombre.Escalera:
                    return "Escalera";
                case JugadasNombre.Color:
                    return "Color";
                case JugadasNombre.Full:
                    return "Full";
                case JugadasNombre.Poquer:
                    return "Poquer";
                case JugadasNombre.Corrida:
                    return "Corrida";
                case JugadasNombre.FlorImperial:
                    return "FlorImperial";
                default:
                    return "CartaAlta";
            }
        }
        
    }
}

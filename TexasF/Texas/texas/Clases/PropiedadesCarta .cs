﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class PropiedadesCarta
    {
        Color color;
        Palo palo;
        public PropiedadesCarta()
        {

        }
        public PropiedadesCarta(Color color, Palo palo)
        {
            Color = color;
            Palo = palo;
        }

        public Color Color { get => color; set => color = value; }
        public Palo Palo { get => palo; set => palo = value; }
    }
}

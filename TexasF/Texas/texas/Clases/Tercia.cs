﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class Tercia
    {
        string nombreJugada = JugadasNombre.Tercia.ToString();
        int valorJugada = JugadasValor.Tercia.getInt();
        int de;

        public Tercia(int de)
        {
            De = de;
        }

        public int De { get => de; set => de = value; }
        public string NombreJugada { get => nombreJugada; set => nombreJugada = value; }
        public int ValorJugada { get => valorJugada; set => valorJugada = value; }
    }
}

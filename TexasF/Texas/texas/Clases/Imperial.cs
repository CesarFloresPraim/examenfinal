﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;
namespace Texas.Clases
{
    class Imperial
    {
        string nombreJugada = JugadasNombre.FlorImperial.ToString();
        int valorJugada = JugadasValor.FlorImperial.getInt();
        int numeroMenor;

        public string NombreJugada { get => nombreJugada; set => nombreJugada = value; }
        public int ValorJugada { get => valorJugada; set => valorJugada = value; }
        public int NumeroMayor { get => numeroMenor; set => numeroMenor = value; }

        public Imperial(int numeroMenor)
        {
            NumeroMayor = numeroMenor;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;
namespace Texas.Clases
{
    class Jugadas
    {
        List<CartaAlta> listaCartaAlta = new List<CartaAlta>();
        List<Par> listaPares = new List<Par>();
        List<Par> listaTercia = new List<Par>();
        List<Par> listaPoquer = new List<Par>();

        internal List<Par> ListaPares { get => listaPares; set => listaPares = value; }
        internal List<Par> ListaTercia { get => listaTercia; set => listaTercia = value; }
        internal List<Par> ListaPoquer { get => listaPoquer; set => listaPoquer = value; }
        internal List<CartaAlta> ListaCartaAlta { get => listaCartaAlta; set => listaCartaAlta = value; }
    }
}

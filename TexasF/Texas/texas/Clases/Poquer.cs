﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class Poquer
    {
        string nombreJugada = JugadasNombre.Poquer.ToString();
        int valorJugada = JugadasValor.Poquer.getInt();
        int de;

        public Poquer(int de)
        {
            this.De = de;
        }

        public int De { get => de; set => de = value; }
    }
}

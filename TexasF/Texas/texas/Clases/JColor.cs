﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;
namespace Texas.Clases
{
    class JColor
    {
        string nombreJugada = JugadasNombre.Color.ToString();
        int valorJugada = JugadasValor.Color.getInt();
        int numeroMayor;

        public string NombreJugada { get => nombreJugada; set => nombreJugada = value; }
        public int ValorJugada { get => valorJugada; set => valorJugada = value; }
        public int NumeroMayor { get => numeroMayor; set => numeroMayor = value; }

        public JColor(int numeroMayor)
        {
            NumeroMayor = numeroMayor;
        }
    }
}

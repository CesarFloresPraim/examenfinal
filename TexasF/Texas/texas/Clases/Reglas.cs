using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texas.Enums; 

namespace Texas.Clases
{
    class Reglas
    {
        int numeroJugadores;
        int jugadorEnTurno = 0;
        bool repartir3 = false;
        bool repartir1 = false;
        bool continuar = false;
        bool continuar2 = false;
        bool repartirUltima = false;


        public void siguienteJugador(Jugadores jugadores)
        {
            jugadorEnTurno++;
            if (jugadorEnTurno == jugadores.ListaJugadores.Count)
            {
                jugadorEnTurno = 0;
            }
        }
        public void confirmarJuego(Jugadores jugadores)
        {
            DialogResult entrarJuego = MessageBox.Show("¿Quieres entrar el juego?", "Confirmar", MessageBoxButtons.YesNo);
            switch (entrarJuego)
            {
                case DialogResult.Yes:
                    jugadores.ListaJugadores[JugadorEnTurno].SeguirJugando = true;
                    jugadorEnTurno++;
                    if (jugadorEnTurno == jugadores.ListaJugadores.Count)
                    {
                        jugadorEnTurno = 0;

                    }
                    break;
                case DialogResult.No:
                    jugadores.ListaJugadoresEliminados.Add(jugadores.ListaJugadores[JugadorEnTurno]);
                    jugadores.ListaJugadores.Remove(jugadores.ListaJugadores[JugadorEnTurno]);
                    if (jugadorEnTurno == jugadores.ListaJugadores.Count)
                    {
                        jugadorEnTurno = 0;

                    }
                    break;
            }

            foreach (Jugador jugador in jugadores.ListaJugadores)
            {
                if (jugador.SeguirJugando == false)//Si encuentra 1 jugador que aun no se le preguna si quiere jugar jugando
                {
                    Repartir3 = false; // no reparte 3
                    repartir1 = false;
                    RepartirUltima = false;

                }
                else
                {
                    Repartir3 = true; //reparte 3
                    repartir1 = true;
                    RepartirUltima = true;


                }
            }


        }
        public void VerficarJugadas(Jugadores jugadores, Dealer dealer)
        {
            //Para cada jugador agregarle las cartas del dealer y las suyas a lista final
            foreach (Jugador jugador in jugadores.ListaJugadores)
            {
                foreach (Carta carta in jugador.CartasJugador)
                {
                    jugador.CartasFinales.Add(carta);
                }
                foreach (Carta carta in dealer.CartasDealer)
                {
                    jugador.CartasFinales.Add(carta);
                }
                OrdenarListaCartas(jugador.CartasFinales, jugador);
                VerificarIguales(jugador.CartasFinales, jugador);
                VerificarCartaAlta(jugador);
                VerificarEscaleraCorridaColor(jugador.CartasFinales, jugador);
                
            }

        }
        private void VerificarIguales(List<Carta> cartasFinales, Jugador jugador)
        {

            int count=0;
            //Cuenta el numero de cartas de cada numero 
            for(int i=2; i<=14; i++)
            {
                for(int j=0;j<cartasFinales.Count; j++)
                {
                    if(i == cartasFinales[j].Numero)
                    {
                        count++;
                    }
                }
                if (count == 4)
                {
                    //crea poker
                    jugador.ListaPoquer.Add(new Poquer(i));

                }
                else if (count == 3)
                {
                    //crea tercia
                    jugador.ListaTercia.Add(new Tercia(i));

                }
                else if (count == 2)
                {
                    //crear par
                    jugador.ListaPar.Add(new Par(i));

                }
                count = 0;
            }
           
        }
        private void VerificarCartaAlta(Jugador jugador)
        {
            if(jugador.CartasJugador[0].Numero > jugador.CartasJugador[1].Numero)
            {
                jugador.ListaCartaAlta.Add(new CartaAlta(jugador.CartasJugador[0].Numero));
            } else
            {
                jugador.ListaCartaAlta.Add(new CartaAlta(jugador.CartasJugador[1].Numero));

            }

        }
        private void OrdenarListaCartas(List<Carta> cartasFinales, Jugador jugador)
        {
            for(int i = 0; i < cartasFinales.Count; i++)
            {
                for(int j = i+1; j < cartasFinales.Count; j++)
                {
                    if (cartasFinales[i].Numero > cartasFinales[j].Numero)
                    {
                        Carta temp;
                        temp = cartasFinales[i];
                        cartasFinales[i] = cartasFinales[j];
                        cartasFinales[j] = temp;
                    }
                    
                }

            }
        }
        private void VerificarEscaleraCorridaColor(List<Carta> cartasFinales, Jugador jugador)
        {
            int countEscalera = 1, countColor = 1;
            List<Carta> tempCorazones = new List<Carta>();
            List<Carta> tempDiamantes = new List<Carta>();
            List<Carta> tempTreboles = new List<Carta>();
            List<Carta> tempEspadas = new List<Carta>();

            int menorEscalera = 0, mayorColor = 0;
            //Escalera
            for (int i = cartasFinales.Count - 1; i > 0; i--)
            {
                int j = i - 1;
                if (cartasFinales[i].Numero == cartasFinales[j].Numero)
                {

                }
                else if (cartasFinales[i].Numero - 1 == cartasFinales[j].Numero)
                {
                    int k = j;
                    menorEscalera = cartasFinales[k].Numero;
                    countEscalera++;

                    if (countEscalera == 5)
                    {
                        jugador.ListaEscalera.Add(new Escalera(menorEscalera));

                    }

                }
                else
                {
                    countEscalera = 1;
                }
            }

            //Corrida

            foreach(Carta carta in cartasFinales)
            {
                if(carta.Propiedades.Palo == Palo.Corazon)
                {
                    tempCorazones.Add(carta);
                }
                else if(carta.Propiedades.Palo == Palo.Trebol)
                {
                    tempTreboles.Add(carta);
                }
                else if (carta.Propiedades.Palo == Palo.Diamante)
                {
                    tempDiamantes.Add(carta);
                }
                else if (carta.Propiedades.Palo == Palo.Espadas)
                {
                    tempEspadas.Add(carta);
                }
            }


            if (tempCorazones.Count >= 5)
            {
                jugador.ListaColor.Add(new JColor(tempCorazones.Last().Numero));
                corridaPalo(tempCorazones,jugador);

            } else if (tempTreboles.Count >= 5)
            {
                jugador.ListaColor.Add(new JColor(tempTreboles.Last().Numero));
                corridaPalo(tempTreboles, jugador);

            } else if (tempDiamantes.Count >= 5)
            {
                jugador.ListaColor.Add(new JColor(tempDiamantes.Last().Numero));
                corridaPalo(tempDiamantes, jugador);

            } else if (tempEspadas.Count >= 5)
            {
                jugador.ListaColor.Add(new JColor(tempEspadas.Last().Numero));
                corridaPalo(tempEspadas, jugador);
            }

           

        }
        private void corridaPalo(List<Carta> tempPalo, Jugador jugador)

        {
            int menorCorrida = 0, countCorrida = 1;
            for (int i = tempPalo.Count - 1; i > 0; i--)
            {
                int j = i - 1;
                if (tempPalo[i].Numero == tempPalo[j].Numero)
                {

                }
                else if (tempPalo[i].Numero - 1 == tempPalo[j].Numero)
                {
                    int k = j;
                    menorCorrida = tempPalo[k].Numero;
                    countCorrida++;

                    if (countCorrida == 5)
                    {
                        if (menorCorrida == 10)
                        {
                            jugador.ListaImperial.Add(new Imperial(menorCorrida));

                        }
                        else
                        {
                            jugador.ListaCorrida.Add(new Corrida(menorCorrida));

                        }

                    }

                }
                else
                {
                    countCorrida = 1;
                }
            }
        }

 
        public int NumeroJugadores { get => numeroJugadores; set => numeroJugadores = value; }
        public int JugadorEnTurno { get => jugadorEnTurno; set => jugadorEnTurno = value; }
        public bool Repartir3 { get => repartir3; set => repartir3 = value; }
        public bool Repartir1 { get => repartir1; set => repartir1 = value; }
        public bool RepartirUltima { get => repartirUltima; set => repartirUltima = value; }
        public bool Continuar { get => continuar; set => continuar = value; }
        public bool Continuar2 { get => continuar2; set => continuar2 = value; }
    }
}

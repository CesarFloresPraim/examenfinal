﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;
namespace Texas.Clases
{
    class Corrida
    {
        string nombreJugada = JugadasNombre.Corrida.ToString();
        int valorJugada = JugadasValor.Corrida.getInt();
        int numeroMayor;

        public string NombreJugada { get => nombreJugada; set => nombreJugada = value; }
        public int ValorJugada { get => valorJugada; set => valorJugada = value; }
        public int NumeroMayor { get => numeroMayor; set => numeroMayor = value; }

        public Corrida(int numeroMayor)
        {
            NumeroMayor = numeroMayor;
        }
    }
}

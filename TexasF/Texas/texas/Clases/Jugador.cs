using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class Jugador
    {
        int identificador, puntos;
        string jugada;
        List<Carta> cartasJugador;
        bool seguirJugando = false;
        List<Carta> cartasFinales = new List<Carta>();
        List<Par> listaPar = new List<Par>();
        List<Tercia> listaTercia = new List<Tercia>();
        List<Poquer> listaPoquer = new List<Poquer>();
        List<CartaAlta> listaCartaAlta = new List<CartaAlta>();
        List<Corrida> listaCorrida = new List<Corrida>();
        List<Escalera> listaEscalera = new List<Escalera>();
        List<JColor> listaColor = new List<JColor>();
        List<Imperial> listaImperial = new List<Imperial>();






        public Jugador(int identificador, List<Carta> cartasJugadorr)
        {
            Identificador = identificador;
            CartasJugador = new List<Carta>();

            
        }
        
        public int Identificador { get => identificador; set => identificador = value; }
        public bool SeguirJugando { get => seguirJugando; set => seguirJugando = value; }
        public int Puntos { get => puntos; set => puntos = value; }
        public string Jugada { get => jugada; set => jugada = value; }
        internal List<Carta> CartasJugador { get => cartasJugador; set => cartasJugador = value; }
        internal List<Carta> CartasFinales { get => cartasFinales; set => cartasFinales = value; }
        internal List<Par> ListaPar { get => listaPar; set => listaPar = value; }
        internal List<Tercia> ListaTercia { get => listaTercia; set => listaTercia = value; }
        internal List<Poquer> ListaPoquer { get => listaPoquer; set => listaPoquer = value; }
        internal List<CartaAlta> ListaCartaAlta { get => listaCartaAlta; set => listaCartaAlta = value; }
        internal List<JColor> ListaColor { get => listaColor; set => listaColor = value; }
        internal List<Corrida> ListaCorrida { get => listaCorrida; set => listaCorrida = value; }
        internal List<Escalera> ListaEscalera { get => listaEscalera; set => listaEscalera = value; }
        internal List<Imperial> ListaImperial { get => listaImperial; set => listaImperial = value; }
    }
}

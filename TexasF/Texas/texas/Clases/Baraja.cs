using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class Baraja
    {
        List<Carta> barajaOrdenada = new List<Carta>();
        List<Carta> cartasQuemadas = new List<Carta>();
        Carta carta, cartaTemp;
        Random rand = new Random();

        public Baraja()
        {
          
        }
        public List<Carta> CrearBaraja()
        {
            for (int i = 2; i <= 14; i++) //Crar numero para carta
            {
                for (int j = 0; j < 4; j++)  //Crear palo para cada carta
                {
                    if (j == 0)
                    {
                        carta = new Carta(i, new PropiedadesCarta(Color.Negra, Palo.Trebol));
                        barajaOrdenada.Add(carta);
                    }
                    else if (j == 1)
                    {
                        carta = new Carta(i, new PropiedadesCarta(Color.Roja, Palo.Diamante));
                        barajaOrdenada.Add(carta);
                    }
                    else if (j == 2)
                    {
                        carta = new Carta(i, new PropiedadesCarta(Color.Negra, Palo.Espadas));
                        barajaOrdenada.Add(carta);
                    }
                    else if (j == 3)
                    {
                        carta = new Carta(i, new PropiedadesCarta(Color.Roja, Palo.Corazon));
                        barajaOrdenada.Add(carta);
                    }

                }
            }
            return barajaOrdenada;
        }


        public List<Carta> Revolver(List<Carta> barajaRevolver)
        {
            for(int i = barajaRevolver.Count-1; i>0; i--)
            {
                int cartaAzar = rand.Next(barajaRevolver.Count);
                cartaTemp = barajaRevolver[i];

                barajaRevolver[i] = barajaRevolver[cartaAzar];
                barajaRevolver[cartaAzar] = cartaTemp;
            }
            return barajaRevolver;
        }

    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas.Clases
{
    class Carta
    {
        int numero;
        PropiedadesCarta propiedades;

        public Carta(int numero, PropiedadesCarta propiedadess)
        {
            this.numero = numero;
            propiedades = new PropiedadesCarta();
            propiedades.Color = propiedadess.Color;
            propiedades.Palo = propiedadess.Palo;
        }
        public int Numero { get => numero; set => numero = value; }
        internal PropiedadesCarta Propiedades { get => propiedades; set => propiedades = value; }
    }
}
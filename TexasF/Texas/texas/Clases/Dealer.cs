using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;
using System.Windows.Forms;
namespace Texas.Clases
{
    class Dealer
    {
        Carta cartaRepartir;
        List<Carta> cartasDealer = new List<Carta>();

        internal List<Carta> CartasDealer { get => cartasDealer; set => cartasDealer = value; }

        public void Repartir(List<Carta> barajaRevuelta, int identificadorJugador,Jugadores jugadores)
        {
            cartaRepartir = barajaRevuelta.Last();//Toma la ultima carta
            barajaRevuelta.Remove(barajaRevuelta.Last());//Quita la ultima carta
            jugadores.ListaJugadores[identificadorJugador].CartasJugador.Add(cartaRepartir);//Agregar carta a jugador en turno
        }

        public bool SacarCartas3(List<Carta> barajaRevuelta)
        {

                cartaRepartir = barajaRevuelta.Last();//Toma la ultima carta
                barajaRevuelta.Remove(barajaRevuelta.Last());//Quita la ultima carta
                CartasDealer.Add(cartaRepartir);
            
            return true;
        }
        public void QuemarCartas1(List<Carta> barajaRevuelta)
        {
            barajaRevuelta.Remove(barajaRevuelta.Last());//Quita la ultima carta
        }
        public void SacarCarta1(List<Carta> barajaRevuelta)
        {

            cartaRepartir = barajaRevuelta.Last();//Toma la ultima carta
            barajaRevuelta.Remove(barajaRevuelta.Last());//Quita la ultima carta
            CartasDealer.Add(cartaRepartir);
        }
        public void voltearCartas(PictureBox carta1, PictureBox carta2)
        {
            carta1.Image = Texas.Properties.Resources.Reves;
            carta2.Image = Texas.Properties.Resources.Reves;

        }
        public void mostrarCartas(PictureBox carta1, Carta carta)
        {
            int numero = carta.Numero;
            string palo = carta.Propiedades.Palo.ToString();
            switch (numero)
            {
                case 14:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources.ace_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources.ace_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources.ace_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources.ace_of_diamonds;
                            break;
                    }
                    break;

                case 2:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._2_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._2_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._2_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._2_of_diamonds;
                            break;
                    }
                    break;

                case 3:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._3_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._3_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._3_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._3_of_diamonds;
                            break;

                    }
                    break;

                case 4:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._4_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._4_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._4_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._4_of_diamonds;
                            break;
                    }
                    break;

                case 5:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._5_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._5_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._5_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._5_of_diamonds;
                            break;
                    }
                    break;
                case 6:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._6_of_spades;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._6_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._6_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._6_of_diamonds;
                            break;
                    }
                    break;

                case 7:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._7_of_spades;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._7_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._7_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._7_of_diamonds;
                            break;
                    }
                    break;

                case 8:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._8_of_spades;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._8_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._8_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._8_of_diamonds;
                            break;
                    }
                    break;

                case 9:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._9_of_spades;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._9_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._9_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._9_of_diamonds;
                            break;
                    }
                    break;

                case 10:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources._10_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources._10_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources._10_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources._10_of_diamonds;
                            break;
                    }
                    break;

                case 11:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources.jack_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources.jack_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources.jack_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources.jack_of_diamonds;
                            break;
                    }
                    break;

                case 12:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources.queen_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources.queen_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources.queen_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources.queen_of_diamonds;
                            break;
                    }
                    break;
                case 13:
                    switch (palo)
                    {
                        case "Corazon":
                            carta1.Image = Texas.Properties.Resources.king_of_hearts;
                            break;
                        case "Espadas":
                            carta1.Image = Texas.Properties.Resources.king_of_spades;
                            break;
                        case "Trebol":
                            carta1.Image = Texas.Properties.Resources.king_of_clubs;
                            break;
                        case "Diamante":
                            carta1.Image = Texas.Properties.Resources.king_of_diamonds;
                            break;
                    }
                    break;
            }

        }

        public List<Jugador> VerificaGanador(Jugadores jugadores, Dealer dealer, Reglas reglas)
        {

            foreach(Jugador jugador in jugadores.ListaJugadores)
            {
                if(jugador.ListaImperial.Count > 0)
                {
                    jugador.Puntos = JugadasValor.FlorImperial.getInt();
                    jugador.Jugada = JugadasNombre.FlorImperial.getString();
                    
                } else if(jugador.ListaCorrida.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Corrida.getInt();
                    jugador.Jugada = JugadasNombre.Corrida.getString();
                }
                else if (jugador.ListaPoquer.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Poquer.getInt();
                    jugador.Jugada = JugadasNombre.Poquer.getString();
                }
                else if (jugador.ListaTercia.Count > 0 && jugador.ListaPar.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Full.getInt();
                    jugador.Jugada = JugadasNombre.Full.getString();
                }
                else if (jugador.ListaColor.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Color.getInt();
                    jugador.Jugada = JugadasNombre.Color.getString();
                }
                else if (jugador.ListaEscalera.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Escalera.getInt();
                    jugador.Jugada = JugadasNombre.Escalera.getString();
                }
                else if (jugador.ListaTercia.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Tercia.getInt();
                    jugador.Jugada = JugadasNombre.Tercia.getString();
                }
                else if (jugador.ListaPar.Count > 1)
                {
                    jugador.Puntos = JugadasValor.Par2.getInt();
                    jugador.Jugada = JugadasNombre.Par2.getString();
                }
                else if (jugador.ListaPar.Count > 0)
                {
                    jugador.Puntos = JugadasValor.Par.getInt();
                    jugador.Jugada = JugadasNombre.Par.getString();
                }
                else if (jugador.ListaCartaAlta.Count > 0)
                {
                    jugador.Puntos = JugadasValor.CartaAlta.getInt();
                    jugador.Jugada = JugadasNombre.CartaAlta.getString();
                }
            }

            return OrdenarPorPuntos(jugadores, dealer, reglas);//ordena jugador por puntos


        }
        private List<Jugador> ChecarEmpates(List<Jugador> tempJugadores, Jugadores jugadores, Dealer dealer, Reglas reglas)
        {
            List<Jugador> jugadoresGanadores = new List<Jugador>();
            int penultimo = jugadores.ListaJugadores.Count - 2;
            if(tempJugadores.Last().Puntos != tempJugadores[penultimo].Puntos) //Checa si el puntaje mas alto no se repite con el anterior
            {
                jugadoresGanadores.Add(tempJugadores.Last());
                return jugadoresGanadores;
            } else
            {
                jugadoresGanadores.Add(tempJugadores.Last());
                for (int i = penultimo; i >= 0; i--)//crea una lista con los jugadores empatados
                {
                    if (tempJugadores.Last().Puntos == jugadores.ListaJugadores[i].Puntos)
                    {
                        jugadoresGanadores.Add(jugadores.ListaJugadores[i]);
                    }

                }
            }
            
            //no puede existir empate con flor imperial

            //empate con corrida
            //solo puede tener una corrida  
            if(tempJugadores.Last().Puntos == Enums.JugadasValor.Corrida.getInt())
            {
                EmpateConCorrida(jugadoresGanadores);
            }
            //empate con poker
            //solo puedes tener un poker
            if(tempJugadores.Last().Puntos == Enums.JugadasValor.Poquer.getInt())
            {
                EmpateConPoquer(jugadoresGanadores);
            }
            //empate con full
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.Full.getInt())
            {
                EmpateConFull(jugadoresGanadores);
            }         
            // empate color
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.Color.getInt())
            {
                EmpateConColor(jugadoresGanadores);
            }
            // empate escalera
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.Escalera.getInt())
            {
                EmpateConEscalera(jugadoresGanadores);
            }
            //empate tercia
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.Tercia.getInt())
            {
                EmpateConTercia(jugadoresGanadores);
            }
            //empate dos pares
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.Par2.getInt())
            {

            }
            //empate pares
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.Par.getInt())
            {
                EmpateConPar(jugadoresGanadores);      
            }
            //empate carta alta
            if (tempJugadores.Last().Puntos == Enums.JugadasValor.CartaAlta.getInt())
            {
                EmpateConCartaAlta(jugadoresGanadores);
            }
            return jugadoresGanadores;
            
        }
        private List<Jugador> EmpateConCorrida(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> CorridaMasAlta = new List<Jugador>();
            foreach (Jugador jugador in jugadoresGanadores)
            {
                CorridaMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo de la carta mas alta de su corrida
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (CorridaMasAlta[i].ListaCorrida.Last().NumeroMayor > CorridaMasAlta[j].ListaCorrida.Last().NumeroMayor)
                    {
                        Jugador temp;
                        temp = CorridaMasAlta[i];
                        CorridaMasAlta[i] = CorridaMasAlta[j];
                        CorridaMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(CorridaMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConPoquer(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> PokerMasAlta = new List<Jugador>();
            foreach (Jugador jugador in jugadoresGanadores)
            {
                PokerMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo del numero de su poker
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (PokerMasAlta[i].ListaPoquer.Last().De > PokerMasAlta[j].ListaPoquer.Last().De)
                    {
                        Jugador temp;
                        temp = PokerMasAlta[i];
                        PokerMasAlta[i] = PokerMasAlta[j];
                        PokerMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(PokerMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConFull(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> TerciaMasAlta = new List<Jugador>();
            foreach (Jugador jugador in jugadoresGanadores)
            {
                TerciaMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo de la carta mas alta de su escalera
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (TerciaMasAlta[i].ListaTercia.Last().De > TerciaMasAlta[j].ListaTercia.Last().De)
                    {
                        Jugador temp;
                        temp = TerciaMasAlta[i];
                        TerciaMasAlta[i] = TerciaMasAlta[j];
                        TerciaMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(TerciaMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConColor(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> ColorMasAlta = new List<Jugador>();
            foreach (Jugador jugador in jugadoresGanadores)
            {
                ColorMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo del numero de su poker
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (ColorMasAlta[i].ListaColor.Last().NumeroMayor > ColorMasAlta[j].ListaColor.Last().NumeroMayor)
                    {
                        Jugador temp;
                        temp = ColorMasAlta[i];
                        ColorMasAlta[i] = ColorMasAlta[j];
                        ColorMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(ColorMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConEscalera(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> EscaleraMasAlta = new List<Jugador>();
            foreach (Jugador jugador in jugadoresGanadores)
            {
                EscaleraMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo de la carta mas alta de su escalera
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (EscaleraMasAlta[i].ListaEscalera.Last().NumeroMayor > EscaleraMasAlta[j].ListaEscalera.Last().NumeroMayor)
                    {
                        Jugador temp;
                        temp = EscaleraMasAlta[i];
                        EscaleraMasAlta[i] = EscaleraMasAlta[j];
                        EscaleraMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(EscaleraMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConTercia(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> TerciaMasAlta = new List<Jugador>();
            foreach(Jugador jugador in jugadoresGanadores)
            {
                TerciaMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo de la carta mas alta de su escalera
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (TerciaMasAlta[i].ListaTercia.Last().De > TerciaMasAlta[j].ListaTercia.Last().De)
                    {
                        Jugador temp;
                        temp = TerciaMasAlta[i];
                        TerciaMasAlta[i] = TerciaMasAlta[j];
                        TerciaMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(TerciaMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConPar(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> ParMasAlta = new List<Jugador>();
            foreach(Jugador jugador in jugadoresGanadores)
            {
                ParMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo de la carta mas alta de su par
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (ParMasAlta[i].ListaPar.Last().De > ParMasAlta[j].ListaPar.Last().De)
                    {
                        Jugador temp;
                        temp = ParMasAlta[i];
                        ParMasAlta[i] = ParMasAlta[j];
                        ParMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(ParMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> EmpateConCartaAlta(List<Jugador> jugadoresGanadores)
        {
            List<Jugador> CartaMasAlta = new List<Jugador>();
            foreach(Jugador jugador in jugadoresGanadores)
            {
                CartaMasAlta.Add(jugador);
            }
            for (int i = 0; i < jugadoresGanadores.Count; i++)//Ordena la lista dependiendo de la carta mas alta de su escalera
            {
                for (int j = i + 1; j < jugadoresGanadores.Count; j++)
                {
                    if (CartaMasAlta[i].ListaCartaAlta.Last().De > CartaMasAlta[j].ListaCartaAlta.Last().De)
                    {
                        Jugador temp;
                        temp = CartaMasAlta[i];
                        CartaMasAlta[i] = CartaMasAlta[j];
                        CartaMasAlta[j] = temp;
                    }

                }
            }
            jugadoresGanadores.Clear();
            jugadoresGanadores.Add(CartaMasAlta.Last());
            return jugadoresGanadores;
        }
        private List<Jugador> OrdenarPorPuntos(Jugadores jugadores, Dealer dealer, Reglas reglas)
        {
            List<Jugador> tempJugadores = jugadores.ListaJugadores;
            
            for (int i = 0; i < jugadores.ListaJugadores.Count; i++)
            {
                for (int j = i + 1; j < jugadores.ListaJugadores.Count; j++)
                {
                    if (tempJugadores[i].Puntos > tempJugadores[j].Puntos)
                    {
                        Jugador temp;
                        temp = tempJugadores[i];
                        tempJugadores[i] = tempJugadores[j];
                        tempJugadores[j] = temp;
                    }

                }
            }
            List<Jugador> Ganadores = ChecarEmpates(tempJugadores, jugadores, dealer, reglas);
            return Ganadores;          
        }
    }

}


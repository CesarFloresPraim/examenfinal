﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class Par
    {
        string nombreJugada = JugadasNombre.Par.ToString();
        int valorJugada = JugadasValor.Par.getInt();
        int de;

        public Par(int de)
        {
            De = de;
        }

        public string NombreJugada { get => nombreJugada; set => nombreJugada = value; }
        public int ValorJugada { get => valorJugada; set => valorJugada = value; }
        public int De { get => de; set => de = value; }
    }
}

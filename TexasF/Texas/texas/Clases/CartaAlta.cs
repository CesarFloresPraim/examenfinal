﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texas.Enums;

namespace Texas.Clases
{
    class CartaAlta 
    {
        
        string nombreJugada = JugadasNombre.CartaAlta.ToString();
        int valorJugada = JugadasValor.CartaAlta.getInt();
        int de;

        public CartaAlta(int de)
        {
            De = de;
        }

        public int De { get => de; set => de = value; }
        public int ValorJugada { get => valorJugada; set => valorJugada = value; }
        public string NombreJugada { get => nombreJugada; set => nombreJugada = value; }
    }
}

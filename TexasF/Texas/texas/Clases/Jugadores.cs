using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas.Clases
{
    class Jugadores
    {
        List<Jugador> listaJugadores = new List<Jugador>();
        List<Jugador> listaJugadoresEliminados = new List<Jugador>();

        internal List<Jugador> ListaJugadores { get => listaJugadores; set => listaJugadores = value; }
        internal List<Jugador> ListaJugadoresEliminados { get => listaJugadoresEliminados; set => listaJugadoresEliminados = value; }

        public void AgregarJugador(Jugador jugador)
        {
            listaJugadores.Add(jugador);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texas.Clases;
using Texas.Enums;

namespace Texas
{
    public partial class Form1 : Form
    {
        Baraja baraja = new Baraja();
        List<Carta> barajaOrdenada = new List<Carta>();
        List<Carta> barajaRevuelta = new List<Carta>();
        Reglas reglas = new Reglas();
        Dealer dealer = new Dealer();
        Jugadores jugadores = new Jugadores();

        public Form1(int numeroJugadores)
        {
            InitializeComponent();
            barajaOrdenada = baraja.CrearBaraja();
            barajaRevuelta = baraja.Revolver(barajaOrdenada);
            reglas.NumeroJugadores = numeroJugadores;
            btn_voltar_cartas.Visible = false;
            //Crear jugadores para el numero de jugadores seleccionados
            for (int i = 1; i <= reglas.NumeroJugadores; i++)
            {

                Jugador jugador = new Jugador(i, null);
                jugadores.AgregarJugador(jugador);
            }
            //Ciclo de 2 para repartir 2 cartas iniciales
            for (int i = 0; i < 2; i++)
            {
                foreach (Jugador jugador in jugadores.ListaJugadores)// A cada jugador repartir una carta
                {
                    dealer.Repartir(barajaRevuelta, reglas.JugadorEnTurno, jugadores);
                    
                        
                    reglas.siguienteJugador(jugadores);
                }

            }
            
        }

        private void btn_check_Click(object sender, EventArgs e)
        {

            reglas.confirmarJuego(jugadores);
            dealer.voltearCartas(pb_carta1, pb_carta2);
            int jugadorActual = reglas.JugadorEnTurno + 1;
            lbl_jugador_act.Text = jugadorActual.ToString();

            if (reglas.Repartir3 == true && reglas.Continuar == false) //verifica que todos los jugadores hayan aceptado
            {
                //agregar 3 cartas a la lista del dealer
                dealer.SacarCartas3(barajaRevuelta);
                dealer.SacarCartas3(barajaRevuelta);
                dealer.SacarCartas3(barajaRevuelta);
                //pinta la carta en su respectivo picturebox
                dealer.mostrarCartas(pictureBox10, dealer.CartasDealer[0]);
                dealer.mostrarCartas(pictureBox11, dealer.CartasDealer[1]);
                dealer.mostrarCartas(pictureBox12, dealer.CartasDealer[2]);
                reglas.Continuar = true;
                reglas.Repartir1 = false;
                reglas.RepartirUltima = false;
                //Establecer el estatus de seguir jugando a falso de todos los jugadores por finalizar turnos
                foreach (Jugador jugador in jugadores.ListaJugadores)
                {
                    jugador.SeguirJugando = false;
                }
            }
            //Reparte la carta 4 solo si ya se han reartido 3 y todos los jugadores ya se le pregunto si quieren jugar
            if(reglas.Repartir1 == true && reglas.Continuar2 == false)
            {
                dealer.QuemarCartas1(barajaRevuelta);//quema 1 carta
                dealer.SacarCarta1(barajaRevuelta);//reparte carta 4
                dealer.mostrarCartas(pictureBox13, dealer.CartasDealer[3]);//muestra carta 4
                reglas.RepartirUltima = false;
                reglas.Continuar2 = true;
                //Establecer el estatus de seguir jugando a falso de todos los jugadores por finalizar turnos
                foreach (Jugador jugador in jugadores.ListaJugadores)
                {
                    jugador.SeguirJugando = false;
                }
            }
            //reparte la ultima carta solo si ya se han repartido 4 y todos los jugadores ya se les pregunte si quieren jugar
            if (reglas.RepartirUltima == true)
            {
                dealer.QuemarCartas1(barajaRevuelta);//quema 1 carta
                dealer.SacarCarta1(barajaRevuelta);//reparte carta 5
                dealer.mostrarCartas(pictureBox14, dealer.CartasDealer[4]);//muestra carta 5
                reglas.VerficarJugadas(jugadores, dealer); //verifica todas las jugadas posibles de cada jugador
                //verifica ganadores e imprime la lista
                StringBuilder ganadores = new StringBuilder();
                ganadores.Append(Environment.NewLine);
                foreach (Jugador ganador in dealer.VerificaGanador(jugadores, dealer, reglas))
                {
                    ganadores.Append(ganador.Identificador);
                    ganadores.Append(Environment.NewLine);
                }
                ganadores.Append(Environment.NewLine);
                List<Jugador> jugadasGanadoras = dealer.VerificaGanador(jugadores, dealer, reglas);
                MessageBox.Show("Eñ jugador ganador es: " +ganadores.ToString()+ " Con:    "+jugadasGanadoras[0].Jugada.ToString());
    
                btn_seguir.Enabled = false;
                btn_ver.Enabled = false;
            }

            if (jugadores.ListaJugadores.Count == 1)
            {
                MessageBox.Show("El jugador ganador es: " + jugadores.ListaJugadores[reglas.JugadorEnTurno].Identificador.ToString());
            }

        }

        private void btn_ver_global_Click(object sender, EventArgs e)
        {

            foreach(Jugador jugador in jugadores.ListaJugadores)
            {
                listBox1.Items.Add("J" + jugador.Identificador.ToString() +" " +jugador.CartasJugador[0].Numero.ToString() + " " + jugador.CartasJugador[1].Numero.ToString());
               
            }


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }

        private void btn_ver_Click(object sender, EventArgs e)
        {
            dealer.mostrarCartas(pb_carta1, jugadores.ListaJugadores[reglas.JugadorEnTurno].CartasJugador[0]); //Muestra carta 1 de jugador en turno
            dealer.mostrarCartas(pb_carta2, jugadores.ListaJugadores[reglas.JugadorEnTurno].CartasJugador[1]);//Muestra carta 2 de jugador en turno
            btn_ver.Visible = false;
            btn_voltar_cartas.Visible = true;
        }

        private void btn_voltear_carta_Click(object sender, EventArgs e)
        {
            dealer.voltearCartas(pb_carta1, pb_carta2);
            btn_ver.Visible = true;
            btn_voltar_cartas.Visible = false;
        }

        private void btn_apuestas_seguir_Click(object sender, EventArgs e)
        {
            
        }
    }
}
